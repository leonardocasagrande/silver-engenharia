<!-- Principal JavaScript do Bootstrap -->
<footer class="bg-dark-gray text-center pt-5"><img class="pb-4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-footer.svg" alt="">
  <div class="d-lg-flex align-items-center justify-content-center pb-lg-5 pt-lg-4">
    <ul class="d-lg-flex">
      <li class=""><a href="<?php echo get_site_url() ?>/empreendimentos">Empreendimentos</a></li>
      <li class="pl-lg-4"><a href="<?php echo get_site_url() ?>/quem-somos">Quem somos</a></li>
      <li class="px-lg-4"><a href="<?php echo get_site_url() ?>/servicos">Serviços</a></li><!-- <li class="px-lg-4"><a href="<?php echo get_site_url() ?>/clientes">Clientes</a></li> -->
      <li class="pr-lg-4"><a href="<?php echo get_site_url() ?>/contato">Contato</a></li>
    </ul>
  </div>
  <div class="separator"></div>
  <div class="endereco"><a href="https://www.google.com/maps/place/Av.+da+Saudade,+404+-+Planalto+do+Sol,+Sumar%C3%A9+-+SP,+13171-320/@-22.8327908,-47.2679184,17z/data=!3m1!4b1!4m5!3m4!1s0x94c8bd108b96405d:0xc26b6175c5a2650a!8m2!3d-22.8327908!4d-47.2657297">Av. da Saudade, 404 - Planalto do Sol - Sumaré/SP - CEP 13172-320</a> <a class="num-telefone" href="tel:+551938749883" target="_blank"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/telefone.png" alt=""> 19 3874.9883</a><a class="num-telefone" href="https://api.whatsapp.com/send?phone=5519996379495" target="_blank"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/whatsapp.png" alt="">19 9 9637.9495</a></div>
  <p class="px-5 py-4">© <?php echo date('Y') ?> Silver Engenharia. Todos os direitos reservados.</p>
</footer>
<div class="text-center d-lg-none py-4 text-white bg-zap banner-fone-sm"><a href="https://api.whatsapp.com/send?phone=5519996379495" target="_blank"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/whatsapp.png" alt=""> <b>VENDAS :</b>19 9 9637.9495</a></div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script><!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script> -->
<script src="<?= get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script> <?php wp_footer(); ?>