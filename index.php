<?php get_header() ?> <section>
  <!-- <div class="banner bg-home">

    <h1 class="">Construindo sonhos</h1>
    <p class=" col-md-6 col-8 p-0">A Silver Engenharia conta com o conhecimento de anos de experiência ao implementar soluções de alta qualidade.</p>

    <a href="#"><i class="color-blue fas fa-angle-down"></i></a>
  </div> --> <?php echo do_shortcode('[rev_slider alias="home"][/rev_slider]') ?> <div class="banner-fone d-none d-lg-block">
    <div class="container">
      <div class="infos">
        <div class="telefone"><a href="tel:+551938749883" target="_blank"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/telefone.png" alt=""> <small>VENDAS:</small> 19 3874.9883</a></div>
        <div class="whatsapp d-none d-lg-block"><a href="https://api.whatsapp.com/send?phone=5519996379495" target="_blank"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/whatsapp.png" alt="">19 9 9637.9495</a></div>
        <div class="email d-none d-lg-block"><a href="mailto:adm1@silverengenharia.com"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/envelope.png" alt=""> E-MAIL</a></div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="col-12 px-0 py-5 lancamentos">
    <h2 class="mb-4 mb-lg-5 titulo-sessao">ÚLTIMOS LANÇAMENTOS</h2> <?php get_template_part('lancamento'); ?> <a href="<?php echo get_site_url() ?>/empreendimentos" class="text-center d-block color-blue font-weight-bold">VER TODOS<a></a></a>
  </div>
</section>
<section class="quem-somos-home pt-md-3 pb-lg-3 bg-gray">
  <div class="text-center d-flex flex-column align-items-center align-items-lg-start text-lg-left pb-5 pt-4 position-relative">
    <h2 class="titulo-sessao mb-4">QUEM SOMOS</h2>
    <div class="d-flex flex-column flex-lg-row align-items-center mb-lg-5 px-4 pb-4">
      <h1 class="pb-3 p-lg-0 mr-lg-5 col-md-6 col-lg-3">Inovação fundada em tradição</h1>
      <p class="col-lg-6 pl-lg-5 col-xl-5 col-md-8 pr-lg-0 m-0">A Silver Engenharia surgiu em 2018, sendo relativamente nova, mas repleta de know-how no desenvolvimento de projetos de alta qualidade, a um preço acessível e com foco na sustentabilidade e respeito ao meio ambiente.</p>
    </div><a class="px-4 py-3 bg-blue align-self-center text-white font-weight-bold" href="<?php echo get_site_url(); ?>/quem-somos">SAIBA MAIS</a>
    <div class="section-detail"></div>
  </div>
</section>
<section class="d-flex bg-gray flex-column pb-lg-5 align-items-center">
  <h2 class="titulo-sessao mt-5 mb-lg-5 mb-4">SERVIÇOS</h2> <?php get_template_part('servicos'); ?> <a class="my-5 btn-servicos-home d-lg-inline" href="<?php echo get_site_url(); ?>/contato">ENTRAR EM CONTATO</a>
</section>
<!-- <section class="bg-gray  ">
  <div class=" clientes-home d-flex flex-column ">
    <h2 class="titulo-sessao align-self-center align-self-lg-start text-left  mb-4">CLIENTES</h2>

    <div class="  px-3 px-lg-0 pb-lg-5 d-flex bg-gray flex-column flex-lg-row  align-items-center align-items-lg-start">

      <div class="text-lg-left text-center d-flex flex-column align-items-center align-items-lg-start col-lg-5 col-xl-4 p-0">
        <h1 class="color-blue col-md-6 pb-lg-3  m-lg-0 col-lg-11  p-0">Quem comprovou
          da qualidade</h1>
        <p class=" py-3  col-md-6 m-lg-0 p-lg-0  col-lg-10">Veja a carteira de clientes Silver Engenharia e descubra quem já conheceu comprovou de toda a qualidade:</p>
      </div>

      <div class="pt-5 px-lg-5 mb-3 col-lg-7   ">

        <div class="cliente-container owl-carousel"> <?php
                                                      $argsClientes = array(
                                                        'post_type' => 'post_clientes',
                                                        'posts_per_page' => 15,
                                                      );
                                                      $post_clientes = new WP_Query($argsClientes);
                                                      if ($post_clientes->have_posts()) :
                                                        while ($post_clientes->have_posts()) : $post_clientes->the_post();
                                                      ?> <img class="item-cliente" src="<?= get_field('logo_cliente'); ?>" alt=""> <?php endwhile;
                                                                                                                                endif; ?> </div>

        <div class="d-lg-flex  justify-content-between pt-lg-5 align-items-center">

          <div class="dot-container text-center py-lg-0 pb-4"></div>

          <a class="btn-clientes  color-blue " href="<?php echo get_site_url(); ?>/clientes">VER TODOS <i class="fas fa-angle-right pl-3"></i></a>
        </div>
      </div>
    </div>
  </div>
</section> -->
<section class="d-flex py-5 bg-blue contato-home flex-column text-center position-relative align-items-center">
  <div class="bg-detail d-none d-lg-block"></div>
  <h2 class="titulo-sessao text-white border-dark mb-4">CONTATO</h2>
  <h1 class="px-3 pb-3 col-lg-4">Conte com a <span class="font-weight-bold">Silver</span> na construção ou incorporação de seu empreendimento imobiliário!</h1>
  <p class="text-white text-center col-lg-4 px-5">Entre em contato e conheça de perto tudo o que podemos fazer pelo sucesso do seu empreendimento:</p>
  <div class="form-home pb-4"> <?php echo do_shortcode('[contact-form-7 id="22" title="Formulário de contato 1"]'); ?> </div>
</section> <?php get_footer() ?>