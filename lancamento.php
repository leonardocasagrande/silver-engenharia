<div class="col-12 px-0 py-5 lancamentos owl-carousel proj-container"> <?php
    $numeroDePosts = -1;
    if (!is_page('projetos')) {
        $numeroDePosts = 3;
    }
    $argsEmpreendimentos = array(
        'post_type' => 'empreendimento',
        'posts_per_page' => $numeroDePosts,
        'orderby' => 'date'
    );
    $empreendimentos = new WP_Query($argsEmpreendimentos);
    //   var_dump($postAtual);
    if ($empreendimentos->have_posts()) :
        while ($empreendimentos->have_posts()) : $empreendimentos->the_post();$taxonomia=get_the_terms(get_the_ID(),'categorias_empreendimento');
    ?> <div class="item p-0 col-md-7 col-xl-8 col-11 margin-center col-lg-7 d-flex"><div class="slide col-6 col-lg-5 p-0" style="background-image:url(<?= get_the_post_thumbnail_url(); ?>)"><img class="d-none d-lg-block" src="<?= get_the_post_thumbnail_url(); ?>" alt=""></div><div class="col-lg-7 col-6 d-flex d-block flex-column justify-content-between p-0"><div class="prime-logo py-lg-5 w-100 d-flex align-items-center justify-content-center"><img src="<?= get_field('logo', get_the_ID()); ?>" alt=""></div><div class="text-item bg-white py-3 p-0 pl-4"><div class="px-3 py-2 my-3"><?= $taxonomia[0]->name ?></div><h3><?php the_title() ?></h3><h4><?php the_field('cidade', get_the_ID()) ?> · <?php the_field('uf', get_the_ID()) ?></h4><p><span class="color-blue">&bull;</span>Apto. <?php the_field('area', get_the_ID()) ?>m²</p></div><a class="p-3 text-white bg-blue" href="<?php the_field('url', get_the_ID(  )) ?>" target="_blank">VER MAIS <i class="fas fa-angle-right pl-3"></i></a></div></div> <?php endwhile;
    endif; ?> </div><div class="d-none flex-column justify-content-center align-items-center pb-5"><div class="dot-lancamento mb-3"></div><div class="nav-container"></div></div>