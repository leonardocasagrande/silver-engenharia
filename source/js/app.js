$(document).ready(function () {
    $('.proj-container').owlCarousel({
        loop: true,
        nav: true,
        dotsContainer: '.dot-lancamento ',
        navContainer: '.nav-container',
        navText: ['<i class="color-blue fas fa-angle-left fa-3x pr-3">', '<i class="color-blue fas fa-angle-right fa-3x"></i>'],
        items: 1,

    })
});

$(document).ready(function () {
    $('.cliente-container').owlCarousel({
        loop: true,
        nav: false,
        autoplay: true,
        margin: 40,
        dotsContainer: '.dot-container ',
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            }

        },

    })
});

$(document).ready(function () {
    $('.single-container').owlCarousel({
        loop: true,
        nav: false,
        autoplay: true,
        items: 1,
        dotsContainer: '.dot-container ',
    })
});

