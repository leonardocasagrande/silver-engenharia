<?php get_header(); ?>



<section class="position-relative py-3 banner-2 bg-gray">

  <h1 class="mb-0 py-4">Empreendimentos</h1>

</section>

<section class="bg-gray projetos-tablet position-relative ">

  <!-- <div class="bg-projetos"></div> -->

    <div class=" py-lg-5 py-3  lancamentos-empreendimento  banner-destaques">
        <div class="container">
            <div class="row">

            
            <?php
            
            $argsEmpreendimentos = array(
                'post_type' => 'empreendimento',
                'posts_per_page' => 3,
                'orderby' => 'date',
                'meta_query' => array(
                    array(
                      'key' => 'destaque',
                      'value' => '1',
                      'compare' => '=='
                    )
                  )
            );
            $empreendimentos = new WP_Query($argsEmpreendimentos);
            //   var_dump($postAtual);
            if ($empreendimentos->have_posts()) :
                while ($empreendimentos->have_posts()) : $empreendimentos->the_post(); $taxonomia=get_the_terms(get_the_ID(),'categorias_empreendimento');
            ?>

            <div class="item  col-lg-4 mb-4 ">

                <div class="imagem-topo p-0">
                    <img class="img-fluid" src="<?= get_the_post_thumbnail_url(); ?>" alt="">
                    <div class="logo-proj">
                        <img class="img-fluid" src="<?= get_field('logo', get_the_ID()); ?>" alt="">
                    </div>
                </div>

                <div class="d-lg-flex d-block flex-column justify-content-between p-0">


                    <a href="<?php the_field('url', get_the_ID()) ?>" target="_blank">
                    <div class="text-item pt-3 bg-white py-lg-3 p-0 pl-4  ">
                        <div class="categoria py-2 mb-3 text-uppercase">Destaque</div>
                        <h3><?php the_title() ?></h3>
                        <h4><?php the_field('cidade', get_the_ID()) ?> · <?php the_field('uf', get_the_ID()) ?></h4>
                        <p><span class="color-blue"></span>Apto. <?php the_field('area', get_the_ID()) ?>m²</p>
                    </div>
                    </a>

                </div>
            </div>
        <?php endwhile;
        endif; ?>

            </div>
        </div>


    </div>

  </div>
  <div class="lancamentos-empreendimento pt-3 pt-lg-5">
      <?php
            $categorias= get_terms(array(
                'taxonomy'=> 'categorias_empreendimento',
                'orderby' => 'name',
                'order'   => 'ASC',
                'parent' => 0
            ));

      ?>
      <div class="container">
      <ul class="nav nav-tabs justify-content-center" role="tablist">


<li class="nav-item">

<a class="nav-link active" id="nav-blog-all" data-toggle="tab" href="#tab-blog-all" role="tab" aria-controls="all" aria-selected="true">Todos</a>

</li>

<?php foreach ($categorias as $categoria) { ?>

<li class="nav-item">

<a class="nav-link" id="nav-blog-<?= $categoria->term_id ?>" data-toggle="tab" href="#tab-blog-<?= $categoria->term_id ?>" role="tab" aria-controls="tab-blog-<?= $categoria->term_id ?>" aria-selected="false"> <?= $categoria->name ?></a>

</li>

<?php } ?>

</ul>
      

        <div class="tab-content pt-4 pb-5 ">
          <!-- tab-pane todos -->
        <div class="tab-pane fade show active" id="tab-blog-all" role="tabpanel" aria-labelledby="nav-blog-all">
          <div class="row posts-blog">
          
            <?php $catquery = new WP_Query(array(
                'post_type'=> 'empreendimento',
                'posts_per_page'=> -1
            )); ?>          
            <?php while ($catquery->have_posts()) : $catquery->the_post();$taxonomia=get_the_terms(get_the_ID(),'categorias_empreendimento') ?>
            <div class="item  col-lg-4 mb-4 ">

                <div class="imagem-topo p-0">
                    <img class="img-fluid" src="<?= get_the_post_thumbnail_url(); ?>" alt="">
                    <div class="logo-proj">
                        <img class="img-fluid" src="<?= get_field('logo', get_the_ID()); ?>" alt="">
                    </div>
                </div>

                <div class="d-lg-flex d-block flex-column justify-content-between p-0">


                    <a href="<?php the_field('url', get_the_ID()) ?>" target="_blank">
                    <div class="text-item pt-3 bg-white py-lg-3 p-0 pl-4  ">
                        <div class="categoria py-2 my-3 text-uppercase"><?= $taxonomia[0]->name ?></div>
                        <h3><?php the_title() ?></h3>
                        <h4><?php the_field('cidade', get_the_ID()) ?> · <?php the_field('uf', get_the_ID()) ?></h4>
                        <p><span class="color-blue"></span>Apto. <?php the_field('area', get_the_ID()) ?>m²</p>
                    </div>
                    </a>

                </div>
            </div>
            <?php endwhile;
            wp_reset_postdata();
            ?>
          </div>
        </div>
          <!-- /tab-pane todos -->

        <?php foreach ($categorias as $categoria) { ?>
        <div class="tab-pane fade" id="tab-blog-<?= $categoria->term_id ?>" role="tabpanel" aria-labelledby="nav-blog-<?= $categoria->term_id ?>">
          
          <div class="row posts-blog">

          <?php $catquery = new WP_Query(array(
                'post_type'=> 'empreendimento',
                'tax_query' => array(
                    array(
                        'taxonomy'=> 'categorias_empreendimento',
                    'field'=> 'slug',
                    'terms' => $categoria->slug
                    )
                ),
                'posts_per_page'=> -1
            ));
            // var_dump($catquery); 
          while ($catquery->have_posts()) : $catquery->the_post(); $taxonomia=get_the_terms(get_the_ID(),'categorias_empreendimento')
          ?>
            <div class="item  col-lg-4 mb-4 ">

<div class="imagem-topo p-0">
    <img class="img-fluid" src="<?= get_the_post_thumbnail_url(); ?>" alt="">
    <div class="logo-proj">
        <img class="img-fluid" src="<?= get_field('logo', get_the_ID()); ?>" alt="">
    </div>
</div>

<div class="d-lg-flex d-block flex-column justify-content-between p-0">


    <a href="<?php the_field('url', get_the_ID()) ?>" target="_blank">
    <div class="text-item pt-3 bg-white py-lg-3 p-0 pl-4  ">
        <div class="categoria py-2 my-3 text-uppercase"><?= $taxonomia[0]->name ?></div>
        <h3><?php the_title() ?></h3>
        <h4><?php the_field('cidade', get_the_ID()) ?> · <?php the_field('uf', get_the_ID()) ?></h4>
        <p><span class="color-blue"></span>Apto. <?php the_field('area', get_the_ID()) ?>m²</p>
    </div>
    </a>

</div>
</div>
          <?php endwhile;
          wp_reset_postdata();
            ?>         
            
          </div>

        </div>
        <?php } ?>
        </div>
      </div>
  </div>

</section>
<section class="bg-emp  ">

    <div class="col-lg-10 d-lg-flex p-0 mb-4 pt-lg-5">
        <div class="aside-form pt-5 col-lg-6 px-4 ">
            <span class="mb-4 ">Entre em <br>contato</span>

            <li class="p-0 pb-3 pb-lg-4">CANAIS DE ATENDIMENTO</li>

            <h3>TELEFONE DE VENDAS</h3>
            <h4 class="pb-3 pb-lg-4"> <a href="tel:+5519999906610">19 9 9990.6610</a></h4>

            <h3 class="pr-5">ESTANDE DE VENDAS E APARTAMENTO DECORADO</h3>
            <h4 class="pb-3 pb-lg-4"> <a href="https://www.google.com/maps/place/Av.+da+Saudade,+404+-+Planalto+do+Sol,+Sumar%C3%A9+-+SP,+13171-320/@-22.8327908,-47.2679184,17z/data=!3m1!4b1!4m5!3m4!1s0x94c8bd108b96405d:0xc26b6175c5a2650a!8m2!3d-22.8327908!4d-47.2657297" target="_blank"> Av. da Saudade, 404 - Planalto do Sol<br>Sumaré/SP - CEP 13172-320</a></h4>

            <h3>E-MAIL DE VENDAS</h3>
            <h4 class=""><a href="mailto:adm1@silverengenharia.com">adm1@silverengenharia.com</a></h4>
            <h4 class="pb-5"><a href="mailto:adm2@silverengenharia.com">adm2@silverengenharia.com</a></h4>

        </div>

        <div class="form-empreend col-lg-6 pb-5 text-lg-left text-center ">

            <li class="pb-2 d-none d-lg-block">FORMULÁRIO DE INTERESSE</li>

            <input class="col-lg-11 py-2 px-3  px-0 mb-3" type="text" placeholder="Nome">
            <input class="col-lg-11 py-2 px-3  px-0 mb-3" type="text" placeholder="E-mail">
            <input class="col-lg-11 py-2 px-3  px-0 mb-3" type="text" placeholder="Telefone">

            <textarea class="col-lg-11 py-2 px-3  px-0 mb-3" name="Mensagem" id="" cols="30" rows="10" placeholder="Mensagem"></textarea>

            <a class=" " href="#">ENVIAR</a>
        </div>
    </div>


</section>





<?php get_footer(); ?>