<div class="col-12 px-0 py-5  lancamentos owl-carousel proj-container  ">

    <?php
    $numeroDePosts = -1;
    if (!is_page('projetos')) {
        $numeroDePosts = 3;
    }
    $argsEmpreendimentos = array(
        'post_type' => 'empreendimento',
        'posts_per_page' => $numeroDePosts,
        'orderby' => 'date'
    );
    $empreendimentos = new WP_Query($argsEmpreendimentos);
    //   var_dump($postAtual);
    if ($empreendimentos->have_posts()) :
        while ($empreendimentos->have_posts()) : $empreendimentos->the_post();
    ?>

            <div class="item  p-0 col-md-7 col-xl-8 col-11 margin-center col-lg-7 d-lg-flex">

                <div class="slide  col-lg-5 p-0">
                    <img src="<?= get_the_post_thumbnail_url(); ?>" alt="">
                </div>

                <div class="col-lg-7 d-lg-flex d-block flex-column justify-content-between p-0">

                    <div class="prime-logo py-lg-5 w-100 d-lg-flex d-none align-items-center justify-content-center ">
                        <img src="<?= get_field('logo', get_the_ID()); ?>" alt="">
                    </div>

                    <div class="text-item bg-white py-lg-3 p-0 pl-4  ">
                        <div class="px-3 py-2 my-3">LANÇAMENTO</div>
                        <h3><?php the_title() ?></h3>
                        <h4><?php the_field('cidade', get_the_ID()) ?> · <?php the_field('uf', get_the_ID()) ?></h4>
                        <p><span class="color-blue">&bull;</span>Apto. <?php the_field('area', get_the_ID()) ?>m²</p>
                        <a class="p-3  text-white bg-blue" href="<?php the_permalink() ?>">VER MAIS <i class="fas fa-angle-right pl-3"></i></a>
                    </div>

                </div>
            </div>
    <?php endwhile;
    endif; ?>




</div>

<div class="d-flex flex-column justify-content-center align-items-center pb-5">
    <div class="dot-lancamento mb-3 "></div>

    <div class="nav-container "></div>
</div>