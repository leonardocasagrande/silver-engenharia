<?php get_header(); ?>

<section class="position-relative  banner-2 bg-gray">

  <h1 class="mb-0">Clientes</h1>

</section>

<section class=" bg-gray">

  <div class="text-center pb-md-5 d-flex flex-column align-items-center col-12  ">
    <h1 class="color-blue   pb-md-3">Quem comprovou
      da qualidade</h1>
    <p class="col-lg-4 col-md-8 ">Veja a carteira de clientes Silver Engenharia e descubra quem já conheceu comprovou de toda a qualidade:</p>
  </div>

  <div class="clientes-box pb-md-5 col-xl-5 col-lg-7 col-md-9 margin-center flex-wrap  d-flex flex-column flex-md-row justify-content-md-between align-items-center">

    <?php
    $argsClientes = array(
        'post_type' => 'post_clientes',
        'posts_per_page' => 15,
        'orderby' => 'date'
    );
    $post_clientes = new WP_Query($argsClientes);
    if ($post_clientes->have_posts()) :
        while ($post_clientes->have_posts()) : $post_clientes->the_post();
    ?>

    <img class="item-cliente mb-5" src="<?= get_field('logo_cliente'); ?>" alt="">

        <?php endwhile;
    endif; ?>
   
  </div>

  <div class="pb-5  text-center">
    <a class="my-5 btn-servicos-home  d-lg-inline" href="<?php echo get_site_url(); ?>/contato">ENTRAR EM CONTATO</a>
  </div>


</section>




<?php get_footer(); ?>