<!-- Principal JavaScript do Bootstrap -->
<footer class="bg-dark-gray text-center pt-5">

  <img class="pb-4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-footer.svg" alt="">

  <div class="separator"></div>

  <div class="d-lg-flex align-items-center justify-content-center pb-lg-5 pt-lg-4">
    <ul class="d-lg-flex ">
      <li class=""><a href="<?php echo get_site_url() ?>/projetos">Projetos</a></li>
      <li class="px-lg-4"><a href="<?php echo get_site_url() ?>/quem-somos">Quem somos</a></li>
      <li class=""><a href="<?php echo get_site_url() ?>/servicos">Serviços</a></li>
      <li class="px-lg-4"><a href="<?php echo get_site_url() ?>/clientes">Clientes</a></li>
      <li class="pr-lg-4"><a href="<?php echo get_site_url() ?>/contato">Contato</a></li>
    </ul>

    <div class="pt-3 pb-5 p-lg-0">
      <a href="#"><i class="color-blue fab fa-instagram"></i></a>
      <a href="#"><i class="color-blue fab fa-twitter px-4"></i></a>
      <a href="#"><i class="color-blue fab fa-facebook-f"></i></a>
    </div>
  </div>
  <p class="px-5 py-4 ">© 2019 Silver Engenharia. Todos os direitos reservados.</p>

</footer>

<?php wp_footer(); ?>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script> -->

<script src="<?= get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script>


</body>

</html>