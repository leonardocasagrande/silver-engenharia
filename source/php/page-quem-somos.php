<?= get_header(); ?>

<section class="position-relative bg-gray">
  <div class="banner bg-quem-somos ">

    <h1 class="">Quem Somos</h1>


    <h2 class="text-center text-lg-left  col-md-6">Inovação fundada
      em tradição </h2>
  </div>

  <div class="text-center d-lg-none w-100 py-4 text-white bg-blue">
    FALE CONOSCO <b>19 9 8110.4970</b>
  </div>

  <div class="px-4 bg-gray history col-lg-5 pt-2 pb-5">

    <h3 class="color-blue pt-lg-0 pt-3 ">História</h3>

    <p>
      Dois construtores, com mais de 25 anos de experiência na construção civil, comercial, industrial e empreendimentos imobiliários residenciais horizontais e verticais, identificaram uma grande oportunidade de unirem seus potenciais e atender este vasto ramo de construção e incorporação de empreendimentos imobiliários.
    </p>
    <p class="mb-0">
      A Silver Engenharia surgiu em 2018, sendo relativamente nova, mas repleta de know-how no desenvolvimento de projetos de alta qualidade, a um preço acessível e com foco na sustentabilidade e respeito ao meio ambiente.
    </p>
  </div>
</section>

<section class="d-lg-flex">

  <div class="col-lg-6 p-0">
    <img class="img-fluid " src="<?= get_stylesheet_directory_uri(); ?>/dist/img/quem-somos-1.png" alt="">
  </div>

  <div class="py-5 px-4 px-md-5 col-lg-6 margin-auto  py-md-3 flex-column d-lg-flex   text-lg-left text-center">


    <h2 class="  m-lg-0 color-blue pb-lg-4 pb-3">Prontos para
      te atender</h2>

    <p class="m-0 p-0 col-lg-9  ">Cada obra possui sua exigência de forma específica. A Silver Engenharia conta com todo o conhecimento de anos de experiência para identificar dificuldades e implementar soluções de alta qualidade, contando com melhoria contínua e visando a segurança, agilidade e satisfação a cada cliente.</p>

  </div>

</section>

<section class=" bg-gray pt-lg-5 d-lg-flex flex-column align-items-center">

  <div class="bg-section-history   text-md-center text-lg-left d-lg-flex align-items-center  col-lg-9 col-xl-8 mt-lg-5 m-0 p-5">

    <div class="col-lg-5">
      <h2 class="color-blue  pb-3">Poucos anos de
        vida, muitos anos
        de experiência.</h2>

      <p class="text-white ">Apesar de ser nova no mercado, a Silver Engenharia foi fundada com mais de 25 anos de experiência para construir projetos inovadores que atendam as necessidades e expectativas de cada cliente. </p>
    </div>

    <div class="separator my-4"></div>

    <div class="col-lg-5">
      <h2 class="color-blue  pb-3">Dedicação
        completa ao
        seu projeto</h2>

      <p class="text-white ">Cada projeto é único e deve ser tratado como tal. Por isso a equipe Silver Engenharia está em constante contato com o cliente para entender e atender suas necessidades e exigências.</p>
    </div>
  </div>

  <div class="section-separator d-none d-lg-block"></div>
</section>


<section class="bg-gray pb-5 mission position-relative ">

  <div class="py-5 p-lg-0 ">

    <div class="text-center mb-5 px-3 ">
      <h2 class="pb-0 mb-0 mb-md-5 d-md-inline-block">Missão<span class="color-blue">.</span></h2>
      <h2 class="pb-0 mb-0 px-lg-3 d-md-inline-block">Visão<span class="color-blue">.</span></h2>
      <h2 class="pb-3 mb-0 d-md-inline-block">Valores<span class="color-blue">.</span></h2>

      <p class="m-0  text-left col-lg-5 col-md-8 margin-center">Construir com foco na qualidade de cada obra, mantendo controle rigoroso nos programas de qualidade e implantando o conceito de melhoria contínua. Visando a completa satisfação dos clientes e valorizando toda a equipe e sua capacidade técnica em sempre fazer o melhor.</p>
    </div>

    <a class=" mb-4  btn-servicos-home d-table margin-center" href="<?php echo get_site_url(); ?>/contato">ENTRAR EM CONTATO</a>

  </div>

  <div class="quem-somos-detail"></div>


</section>









<?= get_footer(); ?>