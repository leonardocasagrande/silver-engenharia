<!DOCTYPE html>
<html lang="pt_BR">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>
		<?php wp_title(''); ?>
	</title>
	<meta name="robots" content="index, follow">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="theme-color" content="#ffffff">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Barlow&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Noto+Serif+TC:wght@100;300;400;500;700&family=Roboto:wght@100;300;400;500;700&display=swap" rel="stylesheet">
	<link rel="preload" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous">
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css"> -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
	<noscript>
		<link rel="stylesheet" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">
	</noscript>
	<?php wp_head(); ?>
</head>

<body>
	<header class="menu-fixo">
		<nav class="navbar p-0 navbar-expand-md align-items-center">
			<div class="bg-white menuzinho py-3 py-0 d-flex justify-content-between align-items-center w-100"><a href="<?php echo get_site_url(); ?>/"><img class="img-fluid ml-md-0" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.svg" alt="Silver Engenharia"></a>
				<div class="collapse navbar-collapse-sm d-lg-block justify-content-center" id="navbarNavDropdown">
					<ul class="navbar-nav d-md-flex">
						<li class="nav-item"><a class="nav-link" href="<?php echo get_site_url() ?>/empreendimentos">Empreendimentos</a>
						</li>
						<li class="nav-item"><a class="nav-link" href="<?php echo get_site_url(); ?>/quem-somos">Quem Somos</a>
						</li>
						<li class="nav-item"><a class="nav-link" href="<?php echo get_site_url(); ?>/servicos">Serviços</a>
						</li>
						<!-- <li class="nav-item ">
							<a class="nav-link" href="<?php echo get_site_url(); ?>/clientes">Clientes</a>
						</li> -->
						<li class="nav-item"><a class="nav-link" href="<?php echo get_site_url(); ?>/contato">Contato</a>
						</li>
						<li class="px-3 bg-blue d-lg-block d-none"><a href="tel:19981104970">19 9 8110.4970</a>
						</li>
					</ul>
					<div class="text-center pb-5 d-lg-none">
						<div class="endereco"><a href="https://www.google.com/maps/place/Av.+da+Saudade,+404+-+Planalto+do+Sol,+Sumar%C3%A9+-+SP,+13171-320/@-22.8327908,-47.2679184,17z/data=!3m1!4b1!4m5!3m4!1s0x94c8bd108b96405d:0xc26b6175c5a2650a!8m2!3d-22.8327908!4d-47.2657297">Av. da Saudade, 404 - Planalto do Sol - Sumaré/SP - CEP 13172-320</a> <a class="num-telefone" href="tel:+55199999606610" target="_blank"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/telefone-heder.png" alt="">19 9 9990.6610</a>
						</div>
					</div>
				</div>
				<button class="navbar-toggler d-lg-none d-md-block" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"></button>
			</div>
		</nav>
	</header>
</body>

</html>