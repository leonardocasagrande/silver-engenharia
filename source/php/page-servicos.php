<?php get_header(); ?>

<section class="position-relative bg-gray">
  <div class=" banner-2 bg-gray ">

    <h1 class="mb-0">Serviços</h1>

  </div>

  <div class="d-flex bg-gray template-services flex-column align-items-center pt-lg-5">

    <?php get_template_part('servicos'); ?>

    <a class="my-5 btn-servicos-home d-lg-inline" href="<?php echo get_site_url(); ?>/contato">ENTRAR EM CONTATO</a>

  </div>


  <!-- <div class="text-center d-lg-none w-100 py-4 text-white bg-blue">
    FALE CONOSCO <b>19 9 8110.4970</b>
  </div> -->


</section>


<?php get_footer(); ?>