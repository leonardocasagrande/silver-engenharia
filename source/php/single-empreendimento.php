<?php get_header();
the_post(); ?>


<section class="position-relative">


    <div class="empreend-img" style="background-image:url(<?= get_the_post_thumbnail_url(); ?>) "></div>


    <div class="box-logo col-12 col-lg-5 text-white">

        <div class="lancamento ">lançamento</div>

        <div class="d-flex bg-dark-gray py-md-5 my-2 justify-content-around py-4 px-2 align-items-center col-10">
            <h1 class="sr-only"><?php the_title() ?></h1>
            <img class="x" src="<?= get_field('logo'); ?>" alt="">

            <div>
                <h2 class="text-white">CIDADE - UF</h2>
                <span class="color-blue"><?php the_field('cidade') ?> · <?php the_field('uf') ?></span>
            </div>
        </div>

        <p class="text-white pt-lg-3"><?php the_field('endereco') ?> - <?php the_field('cidade') ?> · <?php the_field('uf') ?></p>

    </div>

</section>

<section class="bg-emp pb-5   position-relative text-white">

    <div class="pad-top d-flex flex-lg-row px-0 text-center align-items-center align-items-lg-baseline col-lg-6 flex-column ">
        <ul class="col-lg-3 p-0 m-0 d-lg-flex flex-column align-items-lg-baseline">
            <li><?php the_field('andares') ?> andares<br>
                <h2>com <?php the_field('elevadores') ?> elevadores</h2>
            </li>

        </ul>

        <ul class="col-lg-4 m-0 px-lg-4 mx-3  d-lg-flex flex-column align-items-lg-baseline">
            <li>apartamento<br>
                <h2><?php the_field('quartos') ?> quartos premium</h2>
            </li>

        </ul>

        <ul class="col-lg-3 m-0 p-0  d-lg-flex flex-column align-items-lg-baseline">
            <li>Área <br>
                <h2>com <?php the_field('area') ?>m²</h2>
            </li>

        </ul>
    </div>


    <div class="empreend-sobre d-lg-flex pt-3 justify-content-between  px-4 position-relative">

        <div class="col-lg-5 p-0">
            <span class="col-lg-4 titulo-sobre"> Sobre o empreendimento</span>
        </div>


        <div class="pt-4 p-lg-0 pl-lg-5 pr-0 row col-lg-7">

            <?php the_content(); ?>

            <div class="line-detail"></div>



            <ul class="d-flex  pb-3 justify-content-lg-start p-0">
                <li class="font-weight-bold">APARTAMENTO</li>
            </ul>
            <div class="text-center text-lg-left d-flex justify-content-center flex-wrap pr-0 f-18 info-geral">

                <?php

                // check if the repeater field has rows of data
                if (have_rows('informacoes')) :

                    // loop through the rows of data
                    while (have_rows('informacoes')) : the_row(); ?>

                        <div class="col-lg-6">
                            <h3><?php the_sub_field('informacao'); ?></h3>
                        </div>

                <?php
                    endwhile;

                endif;

                ?>

            </div>

        </div>

    </div>


    <div class="empreend-detail"></div>

</section>

<section class="fotos-empreend pb-3 pb-lg-0">

    <span class="d-lg-none d-block m-4">Veja as fotos do apartamento</span>

    <div class="d-lg-flex flex-lg-row-reverse justify-content-center col-lg-10 margin-center p-0 pt-lg-5">
        <div class=" col-lg-4 p-0">

            <div class="single-container position-relative owl-carousel">

                <?php

                $images = get_field('fotos');

                if ($images) : ?>
                    <?php foreach ($images as $image) : ?>
                        <img class="img-fluid p-0 " src="<?php echo $image['sizes']['thumbnail']; ?>" alt="">

                    <?php endforeach; ?>
                <?php endif; ?>


            </div>

            <div class="dot-container boder-none text-center pt-2 pb-4 pt-lg-5"></div>

        </div>

        <div class="col-lg-7 text-md-center text-lg-left p-0">
            <span class="d-none d-lg-block col-lg-8 mb-lg-5 m-4">Veja as fotos do apartamento</span>
            <ul>

                <li class="font-weight-bold pb-3 pb-lg-4">CONDOMÍNIO</li>

                <?php

                // check if the repeater field has rows of data
                if (have_rows('acomodacoes')) :

                    // loop through the rows of data
                    while (have_rows('acomodacoes')) : the_row(); ?>


                        <li><?php the_sub_field('acomodacao'); ?></li>


                <?php
                    endwhile;

                endif;

                ?>

            </ul>
        </div>
    </div>

</section>


<section class="bg-emp text-white  ">

    <div class="col-lg-10 d-lg-flex p-0 margin-center pt-lg-5">
        <div class="aside-form pt-5 col-lg-6 px-4 ">
            <span class="mb-4 ">Entre em <br>contato</span>

            <li class="col-8 p-0 pb-3 pb-lg-4">CANAIS DE ATENDIMENTO</li>

            <h3>TELEFONE DE VENDAS</h3>
            <h4 class="pb-3 pb-lg-4"> 19 9 8110.4970</h4>

            <h3 class="pr-5">ESTANDE DE VENDAS E APARTAMENTO DECORADO</h3>
            <h4 class="pb-3 pb-lg-4">Av. Autorama, 600 - Americana · SP</h4>

            <h3>E-MAIL DE VENDAS</h3>
            <h4 class="pb-5">adm2@silverengenharia.com</h4>

        </div>

        <div class="form-empreend col-lg-6 pb-5 text-lg-left text-center ">

            <li class="pb-2">FORMULÁRIO DE INTERESSE</li>

            <input class="col-11 py-lg-2 px-lg-3  px-0 mb-3" type="text" placeholder="Nome">
            <input class="col-11 py-lg-2 px-lg-3  px-0 mb-3" type="text" placeholder="E-mail">
            <input class="col-11 py-lg-2 px-lg-3  px-0 mb-3" type="text" placeholder="Telefone">

            <textarea class="col-11 py-lg-2 px-lg-3  px-0 mb-3" name="Mensagem" id="" cols="30" rows="10" placeholder="Mensagem"></textarea>

            <a class=" " href="#">ENVIAR</a>
        </div>
    </div>



    <div class=" veja-empreend container py-5">

        <div class="row">
            <div class="mb-4 col-12 text-center"><span>VEJA TAMBÉM</span></div>



            <?php
            $postAtual = get_the_ID();
            $argsEmpreendimentos = array(
                'post_type' => 'empreendimento',
                'posts_per_page' => 3,
                'post__not_in' => array($postAtual)
            );
            $empreendimentos = new WP_Query($argsEmpreendimentos);
            //   var_dump($postAtual);
            if ($empreendimentos->have_posts()) :
                while ($empreendimentos->have_posts()) : $empreendimentos->the_post();
            ?>
                    <div class="col-lg-4 mb-3">
                        <a href="<?php the_permalink() ?>"><img class="img-fluid" src="<?= get_the_post_thumbnail_url(); ?>" alt=""></a>
                        <div class="bg-white p-3 ">
                            <div class="lancamento mb-3">LANÇAMENTO</div>
                            <h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                            <h4><?php the_field('cidade', get_the_ID()) ?> · <?php the_field('uf', get_the_ID()) ?></h4>
                            <p><span class="color-blue pr-2">&bull;</span>Apto. <?php the_field('area', get_the_ID()) ?>m²</p>

                        </div>
                    </div>
            <?php endwhile;
            endif; ?>




        </div>



    </div>
</section>



<?php get_footer(); ?>