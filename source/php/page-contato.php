<?php get_header(); ?>

<section class="position-relative  banner-2 bg-gray">

  <h1 class="mb-0">Contato</h1>

</section>

<section class=" d-lg-flex contato-body text-center  bg-gray">

  <div class=" text pb-lg-5 text-lg-left px-3">
    <h1 class="py-lg-5 px-0 col-lg-11 ">Conte com a <span class="font-weight-bold">Silver</span>
      na construção ou incorporação de seu empreendimento imobiliário!</h1>

    <p class=" pb-lg-5">Entre em contato e conheça de perto tudo o que podemos fazer pelo sucesso do seu empreendimento:</p>

    <p class=""><i class="far fa-envelope color-blue pr-3 "></i>adm2@silverengenharia.com</p>
    <p class=""><i class="fas fa-phone-alt color-blue pr-3"></i> 9 8110.4970</p>
  </div>

  <div class="form-home col-lg-6 bg-blue px-0 d-flex flex-column justify-content-center align-items-center  pb-4">
    <div class="bg-detail "></div>

    <div class="col-lg-10 col-11 p-0 pt-5">
      <?php echo do_shortcode('[contact-form-7 id="22" title="Formulário de contato 1"]'); ?>
    </div>


  </div>


</section>




<?php get_footer(); ?>