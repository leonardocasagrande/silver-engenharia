<?php get_header(); ?> <section class="position-relative banner-2 bg-gray">
  <h1 class="">Contato</h1>
</section>
<section class="d-lg-flex contato-body text-center">
  <div class="text pb-lg-5 text-lg-left px-3">
    <h1 class="py-lg-5 px-0 col-lg-11 color-dgray mb-4">Conte com a <span class="font-weight-bold">Silver</span> na construção ou incorporação de seu empreendimento imobiliário!</h1>
    <p class="pb-4">Entre em contato e conheça de perto tudo o que podemos fazer pelo sucesso do seu empreendimento:</p>
    <ul class="fa-ul info-contatos d-none d-lg-block">
      <li class="mb-3"><span class="fa-li"><i class="fas fa-map-marker-alt color-blue"></i></span><a href="https://www.google.com/maps/place/Av.+da+Saudade,+404+-+Planalto+do+Sol,+Sumar%C3%A9+-+SP,+13171-320/@-22.8327908,-47.2679184,17z/data=!3m1!4b1!4m5!3m4!1s0x94c8bd108b96405d:0xc26b6175c5a2650a!8m2!3d-22.8327908!4d-47.2657297" target="_blank"> Av. da Saudade, 404 - Planalto do Sol<br>Sumaré/SP - CEP 13172-320</a></li>
      <li class="mb-3"><span class="fa-li"><i class="far fa-envelope color-blue"></i></span><a href="mailto:adm1@silverengenharia.com">adm1@silverengenharia.com</a><br><a href="mailto:adm2@silverengenharia.com">adm2@silverengenharia.com</a></li>
      <li class="mb-3"><span class="fa-li"><i class="fab fa-whatsapp color-blue" aria-hidden="true"></i>
        </span><a href="https://api.whatsapp.com/send?phone=5519996379495" target="_blank">19 9 9637.9495</a></li>
      <li class="mb-3"><span class="fa-li"><i class="fas fa-phone-alt color-blue"></i></span><a href="tel:+551938749883">19 3874.9883</a></li>
    </ul>
  </div>
  <div class="form-home col-lg-6 bg-blue px-0 d-flex flex-column justify-content-center align-items-center pb-4">
    <div class="bg-detail d-none d-lg-block"></div>
    <div class="col-lg-10 col-11 p-0 pt-5"> <?php echo do_shortcode('[contact-form-7 id="22" title="Formulário de contato 1"]'); ?> </div>
  </div>
</section>
<ul class="info-contatos pl-0 text-center py-4 d-lg-none">
  <li class="mb-3"><i class="mr-3 fas fa-map-marker-alt color-blue"></i><a href="https://www.google.com/maps/place/Av.+da+Saudade,+404+-+Planalto+do+Sol,+Sumar%C3%A9+-+SP,+13171-320/@-22.8327908,-47.2679184,17z/data=!3m1!4b1!4m5!3m4!1s0x94c8bd108b96405d:0xc26b6175c5a2650a!8m2!3d-22.8327908!4d-47.2657297" target="_blank"> Av. da Saudade, 404 - Planalto do Sol<br>Sumaré/SP - CEP 13172-320</a></li>
  <li class="mb-3"><i class="mr-3 far fa-envelope color-blue"></i><a href="mailto:adm1@silverengenharia.com">adm1@silverengenharia.com</a><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="mailto:adm2@silverengenharia.com">adm2@silverengenharia.com</a></li>
  <li class="mb-3"><i class="fab fa-whatsapp color-blue" aria-hidden="true"></i>
    <a href="https://api.whatsapp.com/send?phone=5519996379495" target="_blank">19 9 9637.9495</a><br><i class="mr-3 fas fa-phone-alt color-blue"></i><a href="tel:+55 1938749883">19 3874.9883</a>
  </li>
</ul> <?php get_footer(); ?>